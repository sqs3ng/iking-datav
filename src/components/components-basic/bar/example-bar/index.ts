import type { App } from 'vue'
import type { SFCWithInstall } from '@/utils/types'
import { loadAsyncComponent } from '@/utils/async-component'
import ExampleBar from './src/index.vue'

ExampleBar.install = (app: App): void => {
  app.component('VExampleBar', ExampleBar)
  app.component('VExampleBarProp', loadAsyncComponent(() => import('./src/config.vue')))
}

export default ExampleBar as SFCWithInstall<typeof ExampleBar>
